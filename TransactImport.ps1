##*********************************************************************************************************************
##*     Title: ImportTransact.ps1
##*   Summary: Imports ACME generated certificate into Transact system
##* Publisher: Michigan State University
##*   Product: Transaction System Enterprise
##********************************************************************************************************************* 

param(
    [Parameter(Position=0,Mandatory=$true)]
    [string]$thumbprint,
    [Parameter(Position=1,Mandatory=$true)]
    [string]$CachePassword
)

if (Test-Path -Path "E:\Transact\host") {
    $global:hostname = Get-ItemPropertyValue HKLM:\SOFTWARE\Transact\Configuration -Name oracleserverhostaddress
    $oratns = Get-ItemPropertyValue HKLM:\SOFTWARE\Transact\Configuration -Name oracleservicename
    $global:oratns="$global:hostname/$oratns"
    $global:version = $false
    $global:TransactOrBb = "Transact"
}
elseif(Test-Path -Path "E:\Blackboard\host") {
    $global:hostname = Get-ItemPropertyValue HKLM:\SOFTWARE\WOW6432Node\Blackboard\Transact\Configuration -Name oracleserverhostaddress
    $oratns = Get-ItemPropertyValue HKLM:\SOFTWARE\WOW6432Node\Blackboard\Transact\Configuration -Name oracleservicename
    $global:oratns = "$global:hostname/$oratns"
    $global:version = $true
    $global:TransactOrBb = "Blackboard"
}
else{
    Write-Host "There was a problem with the directory check - please reach support."
    $global:oratns = "/"
}

$mypwd = ConvertTo-SecureString -String $CachePassword -Force -AsPlainText
Set-Location "E:\$global:TransactOrBb\Host\Server\Tools\OpenSSL"
$filepath = "E:\$global:TransactOrBb\Host\Server\Tools\OpenSSL"

Get-ChildItem -Path cert:\localMachine\my\$thumbprint | Export-PfxCertificate -FilePath $filepath\certificate.pfx -Password $mypwd

write-host ""
write-host "Deleting existing files about to create"
write-host "OK if there are 1 to 3 'file not found' errors."
write-host ""
get-childitem -filter â€œ*.pemâ€ | foreach-object { remove-item $_.fullname -force }
Start-Sleep -s 2

.\openssl pkcs12 -in certificate.pfx -out server.pem -nodes -password pass:$CachePassword

COPY-item -path "E:\$global:TransactOrBb\Host\Server\Tools\OpenSSL\server.pem" -destination "E:\$global:TransactOrBb\Host\Server\Services\server.pem"

if ($global:TransactOrBb -eq "Transact")
{    
    $global:Readers="TransactReadersHost"
    $global:DSAC="TransactDSACHost"
    $global:TIA="TransactTIAHost"
}
else {
    $global:DSAC = "BbTS Dining Services, Activities Host"
    $global:Readers="BbTS Bb Readers Host"
    $global:TIA="BbTS Transaction Integration Agent Host"
}

Restart-Service "$global:Readers"
Restart-Service "$global:DSAC"
Restart-Service "$global:TIA"
