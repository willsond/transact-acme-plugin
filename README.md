## Transact ACME Plugin
A plugin for ACME clients

## Description
This plugin script is designed to work with the ACME client [win-acme](https://www.win-acme.com/) to automate the management of TLS certificates for use with [Transact System Enterprise](https://www.transactcampus.com/). It may be possible to use this with other clients if they support running external scripts, can provide the certificate thumbprint, and other arbritary parameters as arguments to the script.


## Installation
Prequisites:
- Windows Server setup for TSE
- Transact System Enterprise (TSE)
- [win-acme](https://www.win-acme.com/manual/getting-started)

Installing the first two are outside of the scope of this document, although the link for win-acme is for its installation manual.

Once win-acme is installed, copy ImportTransact.ps1 to the scripts directory, usually located at 
```
%Program Files%\win-acme\Scripts
```
By default win-acme does not mark private keys as exportable, TSE requires the keys to be exportable so for this tool to work properly it is required that you edit the configuration file settings.json located in win-acme's program directory. You will need to change PrivateKeyExportable under the Security section to true and add also add it to the CertificateStore section, here is an excerpt of the configuration showing these settings (other settings are not shown for breavity)
```json
  "Security": {
    "RSAKeyBits": 3072,
    "ECCurve": "secp384r1",
    "PrivateKeyExportable": true,
    "EncryptConfig": true
  },
  "Store": {
    "DefaultStore": "My",
    "CertificateStore": {
      "DefaultStore": null,
      "PrivateKeyExportable": true
    },
```
It is important that if you have already tried issuing a certificate using this installation without these settings you must force reissue the certificate for the change to take effect.

## Usage
To use this tool you must first configure your ACME account in win-acme, this tool has only been tested with certificates issued by InCommon (Sectigo) but should work with any issuer supported by TSE, setting up your account at both the provider and in win-acme varies and is out of the scope of this document. In addition ACME protocol relies on the ability to validate control of the domain you are issuing the certificate for, this is usually done using the [HTTP-01](https://datatracker.ietf.org/doc/html/rfc8555#section-6.1) challenge which requires the provider be able to make HTTP requests to your server and may require you to adjust your firewall, there are other challenge types that can be used if your system is not accessible but configuring this is also outside the scope of thie document.

To begin open an administrative command or powershell prompt

```cmd
PS C:\Program Files\win-acme> .\wacs.exe

 A simple Windows ACMEv2 client (WACS)
 Software version 2.1.20.1185 (release, pluggable, standalone, 64-bit)
 Connecting to https://acme.sectigo.com/v2/InCommonRSAOV/...
 Scheduled task looks healthy
 Please report issues at https://github.com/win-acme/win-acme

 N: Create certificate (default settings)
 M: Create certificate (full options)
 R: Run renewals (0 currently due)
 A: Manage renewals (1 total)
 O: More options...
 Q: Quit

 Please choose from the menu: m
```
Select option M
```cmd
 Running in mode: Interactive, Advanced

 Please specify how the list of domain names that will be included in the
 certificate should be determined. If you choose for one of the "all bindings"
 options, the list will automatically be updated for future renewals to
 reflect the bindings at that time.

 1: Read bindings from IIS
 2: Manual input
 3: CSR created by another program
 C: Abort

 How shall we determine the domain(s) to include in the certificate?: <Enter>
```
Select option 1 or press \<Enter\> for default
```cmd
 Please select which website(s) should be scanned for host names. You may
 input one or more site identifiers (comma-separated) to filter by those
 sites, or alternatively leave the input empty to scan *all* websites.

 1: Default Web Site (1 binding)

 Site identifier(s) or <Enter> to choose all: <Enter>
```
Select the option that corresponds to your IIS config or press \<Enter\> for the default
```cmd
 1: yourtseserver.domain.tld (Site 1)

 Listed above are the bindings found on the selected site(s). By default all
 of them will be included, but you may either pick specific ones by typing the
 host names or identifiers (comma-separated) or filter them using one of the
 options from the menu.

 P: Pick bindings based on a search pattern
 R: Pick bindings based on a regular expression
 A: Pick *all* bindings

 Binding identifiers(s) or menu option: <Enter>
```
Select option A or press \<Enter\> for default
```cmd
 1: yourtseserver.domain.tld (Site 1)

 Continue with this selection? (y*/n) - <Enter>
```
Select option 1 or press \<Enter\> for default
```cmd
 Source generated using plugin IIS: yourtseserver.domain.tld

 Friendly name '[IIS] (any site), (any host)'. <Enter> to accept or type desired name: [IIS-ACME] (Transact)
```
Enter a friendly name for display in Windows or press \<Enter\> for default
```cmd
 The ACME server will need to verify that you are the owner of the domain
 names that you are requesting the certificate for. This happens both during
 initial setup *and* for every future renewal. There are two main methods of
 doing so: answering specific http requests (http-01) or create specific dns
 records (dns-01). For wildcard domains the latter is the only option. Various
 additional plugins are available from https://github.com/win-acme/win-acme/.

 1: [http-01] Save verification files on (network) path
 2: [http-01] Serve verification files from memory
 3: [http-01] Upload verification files via FTP(S)
 4: [http-01] Upload verification files via SSH-FTP
 5: [http-01] Upload verification files via WebDav
 6: [dns-01] Create verification records manually (auto-renew not possible)
 7: [dns-01] Create verification records with acme-dns (https://github.com/joohoi/acme-dns)
 8: [dns-01] Create verification records with your own script
 9: [tls-alpn-01] Answer TLS verification request from win-acme
 C: Abort

 How would you like prove ownership for the domain(s)?: <Enter>
```
Select option 2 or press \<Enter\> for default
```cmd
 After ownership of the domain(s) has been proven, we will create a
 Certificate Signing Request (CSR) to obtain the actual certificate. The CSR
 determines properties of the certificate like which (type of) key to use. If
 you are not sure what to pick here, RSA is the safe default.

 1: Elliptic Curve key
 2: RSA key
 C: Abort
```
Select desired option or press \<Enter\> for default
```cmd
 What kind of private key should be used for the certificate?: <Enter>

 When we have the certificate, you can store in one or more ways to make it
 accessible to your applications. The Windows Certificate Store is the default
 location for IIS (unless you are managing a cluster of them).

 1: IIS Central Certificate Store (.pfx per host)
 2: PEM encoded files (Apache, nginx, etc.)
 3: PFX archive
 4: Windows Certificate Store
 5: No (additional) store steps

 How would you like to store the certificate?: <Enter>
```
Select option 4 or press \<Enter\> for default
```cmd
 1: [WebHosting] - Dedicated store for IIS
 2: [My] - General computer store (for Exchange/RDS)
 3: [Default] - Use global default, currently WebHosting

 Choose store to use, or type the name of another unlisted store: 2
```
Select option 2
```cmd
 1: IIS Central Certificate Store (.pfx per host)
 2: PEM encoded files (Apache, nginx, etc.)
 3: PFX archive
 4: Windows Certificate Store
 5: No (additional) store steps

 Would you like to store it in another way too?: <Enter>
```
Select option 5 or press \<Enter\> for default
```cmd
 With the certificate saved to the store(s) of your choice, you may choose one
 or more steps to update your applications, e.g. to configure the new
 thumbprint, or to update bindings.

 1: Create or update bindings in IIS
 2: Start external script or program
 3: No (additional) installation steps

 Which installation step should run first?: <Enter>
```
Select option 1 or press \<Enter\> for default
```cmd
 Use different site for installation? (y/n*) - <Enter>
```
Enter n or press \<Enter\> for default
```cmd
 1: Create or update bindings in IIS
 2: Start external script or program
 3: No (additional) installation steps

 Add another installation step?: 2
```
Select option 2
```cmd
 Description:        Path to script file to run after retrieving the
                     certificate. This may be any executable file or a
                     Powershell (.ps1) script.

 File: C:\Program Files\win-acme\Scripts\ImportTransact.ps1
```
Enter C:\Program Files\win-acme\Scripts\ImportTransact.ps1
```cmd
 {CertCommonName}:   Common name (primary domain name)
 {CachePassword}:    .pfx password
 {CacheFile}:        .pfx full path
 {CertFriendlyName}: Certificate friendly name
 {CertThumbprint}:   Certificate thumbprint
 {StoreType}:        Type of store (e.g. CentralSsl, CertificateStore,
                     PemFiles, ...)
 {StorePath}:        Path to the store
 {RenewalId}:        Renewal identifier
 {OldCertCommonName}: Common name (primary domain name) of the previously
                      issued certificate
 {OldCertFriendlyName}: Friendly name of the previously issued certificate
 {OldCertThumbprint}: Thumbprint of the previously issued certificate

 Description:        Parameters for the script to run after retrieving the
                     certificate. Refer to
                     https://win-acme.com/reference/plugins/installation/script
                     for further instructions.

 Parameters: {CertThumbprint} {CachePassword}
```
Enter  {CertThumbprint} {CachePassword}
```cmd
 1: Create or update bindings in IIS
 2: Start external script or program
 3: No (additional) installation steps

 Add another installation step?: <Enter>
```
Select 3 or press \<Enter\> for default
```cmd
 Using cache. To force a new order within 1 days, run with --force. Beware that you might run into rate limits.
 Downloading certificate [IIS-ACME] (Transact)
 Store with CertificateStore...
 Installation step 1/2: IIS...
 Prevent adding duplicate binding for *:443:yourtseserver.domain.tld
 No bindings have been changed
 Installation step 2/2: Script...
 Script C:\Program Files\win-acme\Scripts\ImportTransact.ps1 starting with parameters 88888888583D686C08AF77888888882A0E49D7A2 ********
 Script finished
 Scheduled task looks healthy
 Adding renewal for [IIS-ACME] (Transact)
 Next renewal scheduled at 2024/7/16 8:55:40
 Certificate [IIS-ACME] (Transact) created

 N: Create certificate (default settings)
 M: Create certificate (full options)
 R: Run renewals (0 currently due)
 A: Manage renewals (2 total)
 O: More options...
 Q: Quit

 Please choose from the menu: q

PS C:\Program Files\win-acme>
```
Once you have finished and if you did not encouter any errors, win-acme will automatically renew and redeploy the updated certificate to both IIS and Transact.
## Support
This tool is not supported by anyone currently, use at your own risk, it has been tested with InCommon (Sectigo) and should work with other supported providers like GoDaddy if they support ACME.

## Contributing
If you are interested in to contributing please reach out to the Systems Engineering Team at the Division of Student Life and Engagement, Michigan State University.

## Authors and acknowledgment
Thank you to whoever wrote the Support Tools for TSE, most of the concepts and integrations are from those.

## License
This is licened under [GLWT](/GWLT)